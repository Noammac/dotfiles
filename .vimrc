" An example for a vimrc file.
"
" Maintainer:	Bram Moolenaar <Bram@vim.org>
" Last change:	2017 Sep 20
"
" To use it, copy it to
"     for Unix and OS/2:  ~/.vimrc
"	      for Amiga:  s:.vimrc
"  for MS-DOS and Win32:  $VIM\_vimrc
"	    for OpenVMS:  sys$login:.vimrc

" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim

if has("vms")
  set nobackup		" do not keep a backup file, use versions instead
else
  set backup		" keep a backup file (restore to previous version)
  if has('persistent_undo')
    set undofile	" keep an undo file (undo changes after closing)
  endif
endif

if &t_Co > 2 || has("gui_running")
  " Switch on highlighting the last used search pattern.
  set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

  " Put these in an autocmd group, so that we can delete them easily.
  augroup vimrcEx
  au!

  " For all text files set 'textwidth' to 100 characters.
  autocmd FileType text setlocal textwidth=100

  augroup END

endif " has("autocmd")

set autoindent		" always set autoindenting on
" Add optional packages.
"
" The matchit plugin makes the % command work better, but it is not backwards
" compatible.
" The ! means the package won't be loaded right away but when plugins are
" loaded during initialization.
if has('syntax') && has('eval')
  packadd! matchit
endif
filetype off                  " required
set noshowmode
set laststatus=2

" Colorscheme
color elflord

" Clipboard support
set clipboard+=unnamedplus

" LaTeX (rubber) macro for compiling
nnoremap <leader>cp  :w<CR>:!rubber -m pdftex --pdf --warn all %<CR>

" LaTeX (lualatex) macro for compiling
nnoremap <leader>cl :w<CR>:!lualatex --halt-on-error %<CR>

" LaTeX (xelatex) macro for compiling
nnoremap <leader>cx :w<CR>:!rubber -m xelatex --pdf --warn all %<CR>

" View PDF macro; '%:r' is current file's root (base) name.
nnoremap <leader>v :!zathura %:r.pdf &<CR><CR>

" Move lines around
noremap - ddp
noremap _ ddkP

" Compile and run java file if inside the designated Java directory
function! JavaCompile()
	if expand('%:e') == "java" && expand('%:p:h') == expand("$JAVA_FOLDER/src")
		!$JAVA_FOLDER/compile.sh %:p
	endif
endfunction

nnoremap <leader>jc :call JavaCompile()<CR>

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" Readline key bindings
Plug 'tpope/vim-rsi'

" ALE linting and completion
Plug 'w0rp/ale'

" Better python highlighting
Plug 'vim-python/python-syntax'

" Better management of surroundings
Plug 'tpope/vim-surround'

" Selectively illuminate other uses of word under the cursor
Plug 'RRethy/vim-illuminate'

" Git integration
Plug 'tpope/vim-fugitive'

" Status line
Plug 'itchyny/lightline.vim'

" All of your Plugins must be added before the following line
call plug#end()            " required

" Brief help
" :PlugInstall    - Install plugins
" :PlugUpdate     - Install or update plugins
" :PlugClean[!]   - Remove unused directories (bang version will clean without prompt)
" :PlugUpgrade    - Upgrade vim-plug itself
" :PlugStatus     - Check the status of plugins
" :PlugDiff       - Examine changes from the previous update and the pending changes
" :PlugSnapshot   - Generate script for restoring the current snapshot of the plugins

" ALE configuration
let g:ale_linters = {
		\ 'python' : [ 'pycodestyle', 'mypy', 'isort' ],
		\ }

" lightline configuration
let g:lightline = {
		\ 'colorscheme': 'darcula',
		\ 'active': {
		\   'left': [ [ 'mode', 'paste'],
		\             [ 'fugitive', 'readonly', 'filename' ] ]
		\ },
		\ 'component': {
		\   'lineinfo': ' %3l:%-2v',
		\ },
		\ 'component_function': {
		\   'readonly': 'LightlineReadonly',
		\   'fugitive': 'LightlineFugitive',
		\   'filename': 'LightlineFilename'
		\ },
		\ 'separator': { 'left': '', 'right': '' },
		\ 'subseparator': { 'left': '', 'right': '' }
		\ }
function! LightlineReadonly() abort
	return &readonly ? '' : ''
endfunction
function! LightlineFugitive() abort
	if exists('*fugitive#head')
		let branch = fugitive#head()
		return branch !=# '' ? ''.branch : ''
	endif
	return ''
endfunction
function! LightlineFilename() abort
	let filename = expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
	let modified = &modified ? ' +' : ''
	return filename . modified
endfunction

" Python configuration
let g:python_highlight_all = 1
" let g:python_highlight_builtins = 1

" vim-illuminate configuration
let g:Illuminate_ftHighlightGroups = {
	\ 'vim': ['vimVar', 'vimString', 'vimLineComment',
	\ 'vimFuncName', 'vimFunction', 'vimUserFunc', 'vimFunc', 'Identifier'],
 	\ 'python': ['Function', 'Comment', 'Special', 'Identifier', 'Structure']
	\ }
hi link illuminatedWord Visual
