" vim:sw=2
" Meant **only** for neovim use.

" Include addons meant for vim
set rtp^='/usr/share/vim/vimfiles'

set mouse=a " Doesn't warrant an explanation

" Put these in an autocmd group, so that you can revert them with:
" ":augroup vimStartup | au! | augroup END"
augroup vimStartup
  au!

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid, when inside an event handler
  " (happens when dropping a file on gvim) and for a commit message (it's
  " likely a different one than last time).
  autocmd BufReadPost *
    \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
    \ |   exe "normal! g`\""
    \ | endif
augroup END

autocmd FileType text setlocal textwidth=100

" Don't close terminal automatically
autocmd TermOpen * set bufhidden=hide

" Backups
set backup   " keep a backup file (restore to previous version)
set undofile " keep an undo file (undo changes after closing)

" Allow for folds in files
set foldmethod=marker

" Switch on highlighting the last used search pattern.
set hlsearch

set termguicolors " I prefer truecolor

augroup buffer_stuff
  set hidden " So buffers don't wipe the moment you switch away

  " Key bindings for switching buffers
  nnoremap <silent> gb :bn<CR>
  nnoremap <silent> gB :bp<CR>

  " Function for the creation of scratch buffers.
  function! Scratch()
    noswapfile hide enew
    setlocal buftype=nofile
    setlocal bufhidden=hide
    file scratch
  endfunction
augroup END

" Preferred tab settings
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

" Use actual tab chars in Makefiles
autocmd FileType make set tabstop=8 shiftwidth=8 softtabstop=0 noexpandtab

" View PDF result; '%:r' is current file's root (base) name.
autocmd FileType tex,rst,asy :nnoremap <leader>pv :!zathura %:r.pdf &<CR><CR>

augroup filetype_js
  autocmd FileType javascript :set sw=2
  autocmd FileType typescript :set sw=2
augroup END

augroup filetype_tex
  " Comfortable editing
  autocmd FileType tex :set linebreak

  " Compile into PDF
  autocmd FileType tex :nnoremap <leader>lp :w<CR>:!latexmk -interaction=nonstopmode -pdflatex %<CR>
  autocmd FileType tex :nnoremap <leader>ll :w<CR>:!latexmk -interaction=nonstopmode -lualatex %<CR>
  autocmd FileType tex :nnoremap <leader>lx :w<CR>:!latexmk -interaction=nonstopmode -xelatex %<CR>

  " Compile into PNG
  autocmd FileType tex :nnoremap <leader>li :w<CR>:!latex %<CR>dvipng %:r.dvi<CR>

  " Clean logs and temp files
  autocmd FileType tex :nnoremap <leader>lc :!latexmk -c %<CR>

augroup END

augroup filetype_asy
  " Compile
  autocmd FileType asy :nnoremap <leader>ac :w<CR>:!asy %<CR>

  " Compile and view
  autocmd FileType asy :nnoremap <leader>av :w<CR>:!asy -V %<CR><CR>
augroup END

augroup filetype_rst
  " Appends a line of equal length to the current one, with all characters
  " replaced. This allows for the easy creation of reST titles.
  autocmd FileType rst :nnoremap <silent> <leader>t :put =substitute(getline('.'), '.', getcharstr(), 'g') <CR>

  " Compile into HTML
  autocmd FileType rst :nnoremap <leader>rh :w<CR>:!rst2html5 --smart-quotes=yes --image-loading=embed % > %:r.html<CR>

  " Compile into PDF
  autocmd FileType rst :nnoremap <leader>rp :w<CR>:!rst2pdf --smart-quotes=2 --inline-footnotes %<CR>

  " View HTML
  autocmd FileType rst :nnoremap <leader>hv :!nightly %:r.html<CR><CR>

  " Indentation settings
  autocmd FileType rst :set sw=2 lbr
augroup END

" Allow work in virtualenvs
let g:python3_host_prog='/usr/bin/python'

call plug#begin('~/.local/share/nvim/plugged')

" The plugin manager
Plug 'junegunn/vim-plug'

" Sensible defaults
Plug 'tpope/vim-sensible'

" Language server
Plug 'neovim/nvim-lspconfig'

" New completion
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

Plug 'L3MON4D3/LuaSnip'
Plug 'saadparwaiz1/cmp_luasnip'

" See function signatures
Plug 'ray-x/lsp_signature.nvim'

" Completions for zsh shell
Plug 'tamago324/cmp-zsh'

" Useful snippets
Plug 'rafamadriz/friendly-snippets'

" ALE linting and completion
" Plug 'dense-analysis/ale'

" Bindings for commenting lines
Plug 'b3nj5m1n/kommentary'

" Readline key bindings
Plug 'tpope/vim-rsi'

" Make tables and align text
Plug 'godlygeek/tabular'

" Better management of surroundings
Plug 'tpope/vim-surround'

" Allows for syntax analysis
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Python syntax highlighting
" Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'}

" Rust highlighting
" Plug 'arzg/vim-rust-syntax-ext', { 'for': 'rust' }

" TOML highlighting
Plug 'cespare/vim-toml', { 'for': 'toml' }

" For kitty config files
Plug 'fladson/vim-kitty'

" Make parentheses easier to distinguish
Plug 'p00f/nvim-ts-rainbow'

" Show indentation lines
Plug 'lukas-reineke/indent-blankline.nvim'

" Git integration
Plug 'tpope/vim-fugitive'

" Status line
set noshowmode
Plug 'itchyny/lightline.vim'

" PaperColor theme
Plug 'NLKNguyen/papercolor-theme'

call plug#end()

let g:vimsyn_embed='l'
set completeopt=menu,menuone,noselect

lua << EOF
-- Set up completion
local cmp = require('cmp')

-- List of symbols
local kind_icons = {
  Class = '∴',
  Color = '#',
  Constant = 'π',
  Constructor = '⬡',
  Enum = 'ↄ',
  EnumMember = 'ⅰ',
  Event = '!',
  Field = '→',
  File = '',
  Folder = '',
  Function = 'ƒ',
  Interface = 'I',
  Keyword = '🗝',
  Method = '𝘮',
  Module = '📦',
  Operator = '±',
  Property = '∷',
  Reference = '⊷',
  -- Snippet = '↲',
  Snippet = '{}',
  Struct = '∅',
  Text = "𝓣",
  TypeParameter = '×',
  Unit = '()',
  Value = '123', 
  Variable = '𝛸',
}

cmp.setup({
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
  formatting = {
    format = function(entry, vim_item)
      vim_item.kind = kind_icons[vim_item.kind]
      vim_item.menu = ({
        buffer = "[B]",
        nvim_lsp = "[LSP]",
        luasnip = "[Snip]",
        path = "[Path]",
        zsh = "[Zsh]",
      })[entry.source.name]
      return vim_item
    end
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
    ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
    ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
    ['<C-y>'] = cmp.config.disable,
    ['<C-e>'] = cmp.mapping({
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    }),
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
  }),
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
    { name = 'path' },
  }, {
    { name = 'buffer' },
  })
})

cmp.setup.cmdline(':', {
  sources = cmp.config.sources({
    { name = 'cmdline' }
    }, {
      { name = 'buffer' }
    })
})

cmp.setup.filetype('zsh', {
  sources = cmp.config.sources({
    { name = 'zsh' },
    { name = 'luasnip' },
    { name = 'path' },
  }, {
    { name = 'buffer' },
  })
})

-- Set up the language servers.
local lspconfig = require('lspconfig')
local signature = require('lsp_signature')

local opts = { noremap = true, silent = true }

vim.api.nvim_set_keymap('n', '<leader>e', '<cmd>lua vim.diagnostic.open_float()<CR>', opts)
vim.api.nvim_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
vim.api.nvim_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)
vim.api.nvim_set_keymap('n', '<leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>', opts)

local on_attach = function(_, bufnr)

  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
  signature.on_attach({
    bind = true,
    handler_opts = {
      border = "single",
    },
    hint_prefix = "",
  }, bufnr)
end

-- nvim-cmp supports additional completion capabilities
local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

local servers = { 'clangd', 'rust_analyzer', 'pyright', 'texlab', 'tsserver', 'hls', 'svelte', }
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end

-- Set up the indentation markers
require("indent_blankline").setup {
  buftype_exclude = {"terminal", "help", "nofile"},
}

-- Set up Tree Sitter
require('nvim-treesitter.configs').setup {
  ensure_installed = { "c", "html", "css", "latex", "javascript", "rust", "vim", "haskell", "svelte", },
  highlight = {
    enable = true,
    -- disable = { "perl" },
    additional_vim_regex_highlighting = false,
  },
  -- Set up rainbow parentheses
  rainbow = {
    enable = true,
    extended_mode = true,
    max_file_lines = nil,
    colors = {
      "#5fafd7",
      "#5faf00",
      "#ffaf00",
      "#af0000",
      "#af87d7",
    }
  }
}
EOF

" Colourscheme
set background=dark
let g:PaperColor_Theme_Options = {
  \  'theme': {
  \    'default': {
  \      'transparent_background': 1
  \      }
  \    }
  \  }

colorscheme PaperColor

" Use deoplete
" let g:deoplete#enable_at_startup = 1
" let g:deoplete#lsp#use_icons_for_candidates = v:true
" call deoplete#custom#option('auto_complete_delay', 100)
" inoremap <silent><expr><c-space> deoplete#complete()
" inoremap <silent><expr><Tab> pumvisible() ? "\<C-n>"
"       \ : (<SID>is_whitespace() ? "\<Tab>" : deoplete#mappings#manual_complete())
" inoremap <expr><S-Tab>  pumvisible() ? "\<C-p>" : "\<C-h>"

" ALE configuration
" let g:ale_linters = {
"   \     'asm':        [ ],
"   \     'python':     [ 'flake8' ],
"   \     'rust':       [ ],
"   \     'tex':        [ 'proselint' ],
"   \     'text':       [ 'proselint' ],
"   \     'typescript': [ 'eslint' ]
"   \  }

" let g:ale_fixers = {
"   \     'css':        [ 'stylelint' ],
"   \     'json':       [ 'jq' ],
"   \     'python':     [ 'isort' ],
"   \     'rust':       [ 'rustfmt' ],
"   \     'typescript': [ 'deno', 'eslint' ]
"   \  }

" lightline configuration
let g:lightline = {
  \     'colorscheme': 'PaperColor',
  \     'active': {
  \             'left': [ [ 'mode', 'paste'],
  \                     [ 'fugitive', 'readonly', 'filename' ] ]
  \     },
  \     'component': {
  \             'lineinfo': ' %3l:%-2v',
  \     },
  \     'component_function': {
  \             'readonly': 'LightlineReadonly',
  \             'fugitive': 'LightlineFugitive',
  \             'filename': 'LightlineFilename'
  \     },
  \     'separator': { 'left': '', 'right': '' },
  \     'subseparator': { 'left': '', 'right': '' }
  \  }

function! LightlineReadonly() abort
  return &readonly ? '' : ''
endfunction

function! LightlineFugitive() abort
  let branch = FugitiveHead()
  return branch !=# '' ? ''.branch : ''
endfunction

function! LightlineFilename() abort
  let filename = expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
  let modified = &modified ? ' +' : ''
  return filename . modified
endfunction

" Semshi configuration
let g:semshi#mark_selected_nodes = 2
let g:semshi#error_sign = v:false
let g:semshi#update_delay_factor = 0.0001

nnoremap <silent> <leader>rr :Semshi rename<CR>

" Tabular configuration
nnoremap <leader>t :Tabularize 

" Rainbow parentheses configuration
let g:rainbow_active = 1
