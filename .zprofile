# Environment Variables
## XDG Base directories
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_STATE_HOME="${HOME}/.local/state"

## Fixing various storage to unclutter the home dir
export ZPLUG_HOME="${XDG_DATA_HOME}/zplug"

export ANDROID_HOME="${XDG_DATA_HOME}/android"

export GNUPGHOME="${XDG_DATA_HOME}/gnupg"

export XINITRC="${XDG_CONFIG_HOME}/X11/xinitrc"
export XSERVERRC="${XDG_CONFIG_HOME}/X11/xserverrc"
export XAUTHORITY="${XDG_CONFIG_HOME}/X11/Xauthority"
export XCOMPOSEFILE="${XDG_CONFIG_HOME}/X11/xcompose"

export KDEHOME="${XDG_CONFIG_HOME}/kde"
export GTK_RC_FILES="${XDG_CONFIG_HOME}/gtk-1.0/gtkrc"
export GTK2_RC_FILES="${XDG_CONFIG_HOME}/gtk-2.0/gtkrc"

export TEXMFHOME="${XDG_DATA_HOME}/texmf"
export TEXMFVAR="${XDG_CACHE_HOME}/texlive/texmf-var"
export TEXMFCONFIG="${XDG_CONFIG_HOME}/texlive/texmf-config"

export GOPATH="${XDG_DATA_HOME}/go"
export CABAL_CONFIG="${XDG_CONFIG_HOME}/cabal/conf"
export CABAL_DIR="${XDG_CACHE_HOME}/cabal"
export RUSTUP_HOME="${XDG_DATA_HOME}/rustup"
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME}/npm/npmrc"

export ASYMPTOTE_HOME="${XDG_CONFIG_HOME}/asy"

## Other stuff.
export GPG_TTY=$TTY
export SSH_AUTH_SOCK="${XDG_RUNTIME_DIR}/ssh-agent.socket"
