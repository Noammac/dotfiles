# User configuration
## PATH modifiers
RUBY_VER="3.0.0" # Better than running Ruby each time, change when you need to
FOLDERS=("/usr/bin/site_perl" "/usr/bin/vendor_perl" "/usr/bin/core_perl" "$HOME/.local/bin" "$HOME/bin" "$HOME/.cargo/bin" "$HOME/.gem/ruby/$RUBY_VER/bin")

for folder in $FOLDERS
do if [[ -d $folder ]]
    then PATH="$PATH:$folder"
fi
done

if [[ -f "$HOME/miniconda3/etc/profile.d/conda.sh" ]]; then source "$HOME/miniconda3/etc/profile.d/conda.sh"; fi

export PATH

export COUNTDOWN_DATE="2023-01-01"

export EDITOR="/bin/nvim" # User selected editor
export DIFFPROG="/bin/nvim -d"
# End User Configuration

# zplug
export ZPLUG_HOME="${XDG_DATA_HOME}/zplug"
source $ZPLUG_HOME/init.zsh

## self-manage
zplug "zplug/zplug", hook-build:'zplug --self-manage'

## oh-my-zsh library
zplug "lib/completion", from:oh-my-zsh, defer:1
zplug "lib/directories", from:oh-my-zsh
zplug "lib/functions", from:oh-my-zsh
zplug "lib/grep", from:oh-my-zsh
zplug "lib/history", from:oh-my-zsh
zplug "lib/key-bindings", from:oh-my-zsh
zplug "lib/misc", from:oh-my-zsh
zplug "lib/termsupport", from:oh-my-zsh
zplug "lib/theme-and-appearance", from:oh-my-zsh, defer:2

## Bundles for the default repo
zplug "plugins/common-aliases", from:oh-my-zsh
zplug "plugins/colored-man-pages", from:oh-my-zsh
zplug "plugins/zoxide", from:oh-my-zsh
zplug "plugins/git", from:oh-my-zsh
zplug "plugins/pip", from:oh-my-zsh
zplug "plugins/virtualenv", from:oh-my-zsh
zplug "plugins/archlinux", from:oh-my-zsh

## Syntax highlighting
zplug "zdharma-continuum/fast-syntax-highlighting", from:github, defer:2

## HSMW and history context searching
zplug "zdharma-continuum/history-search-multi-word", from:github, defer:2

## More completions
zplug "zsh-users/zsh-completions", from:github

## Auto-suggestions
zplug "zsh-users/zsh-autosuggestions", from:github

## Theme
zplug "themes/agnoster", from:oh-my-zsh, as:theme
DEFAULT_USER=noammac
export VIRTUAL_ENV_DISABLE_PROMPT=true

## End bundles

## Check installation
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

## Load plugins
zplug load

## Syntax highlighting configuration
if [[ -e ~/.fsh/overlay.ini && ! -e $FAST_WORK_DIR/theme_overlay.zsh ]]; then fast-theme -q HOME:overlay; fi

# End zplug

# Functions, widgets and keybindings
## Functions
zmodload zsh/zpty
autoload zed
autoload -U tetriscurses

dict() {
    curl "dict://dict.org/d:$*"
}

remnd() {
    at now + $1 $2 <<< "espeak \"${argv[@]:3}\""
}

record() {
    arecord -vv -fdat "$1".wav
    ffmpeg -i "$1".{wav,mp3}
    7z a "$1".{7z,mp3} -p
    shred -u "$1".{wav,mp3}
}

yt() {
    mpv --hwdec=auto --ytdl-format="bv*[height${2:+<=}${2:-<1920}]${3:+[vcodec^=$3]}+ba" "${1:?No URL provided}"
}

avid() {
    mpv --hwdec=auto --ytdl-format="bv*[height${2:+<=}${2:-<1920}][vcodec^=av01]+ba" "${1:?No URL provided}"
}

zman() {
    info zsh --index-search=$*
}

### Functions for agnoster prompt
prompt_conda() {
    if [[ -n "$CONDA_DEFAULT_ENV" && "$CONDA_DEFAULT_ENV" != base ]]
    then
        prompt_segment green black "(${CONDA_DEFAULT_ENV})"
    fi
}

build_prompt() {
    RETVAL=$?
    prompt_status
    prompt_segment cyan black "%*"
    prompt_virtualenv
    prompt_conda
    prompt_context
    prompt_dir
    prompt_git
    prompt_end
}

## Key bindings, executed after oh-my-zsh to work.
bindkey "^[[3;5~" delete-word
bindkey "^[[3;3~" delete-word
bindkey "[3~" backward-delete-word
bindkey "^H" backward-delete-word
bindkey "^[[1;3C" forward-word
bindkey "^[[1;3D" backward-word
# End functions, widgets and key bindings

# Aliases and default programs
## aliases, needs to be executed after oh-my-zsh to assert higher priority
### aliases using already-installed programs
alias quit="exit"
alias py="python3"
alias gdiff="diff -du --color=always"

### aliases using programs that need to be installed
if [[ -x /usr/bin/exa ]]; then alias exs="noglob exa -I *~"; fi
if [[ -x /usr/bin/nvim ]]
then
        alias vim="/usr/bin/nvim"
        alias v="/usr/bin/nvim"
fi
if [[ -x /usr/bin/reflector ]]; then alias pacrepo='sudo reflector --ipv4 --threads 16 -p https -c IL,IT,GB,GR,AT,DE -l 40 -f 20 --sort rate --save /etc/pacman.d/mirrorlist'; fi

## Default programs environment variables
export LESS="-R +Gg"
export MANPAGER="less"
if [ -x $HOME/bin/nightly ]; then export BROWSER=nightly
elif [ -x /opt/firefox/firefox ]; then export BROWSER=/opt/firefox/firefox; fi
# End aliases and default programs

# Startup commands, refer to launch-{tetris,man} and to clock
if [[ -z $NO_STARTUP ]]; then
        neofetch
        dater
fi
